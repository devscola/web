class Page {
  constructor() {
    //Header
    this.xhrCallAttachment('trello-attachment-logo',"https://api.trello.com/1/cards/9nPqrxUD?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    //body

    //Que es la devscola
    this.xhrCallTitle('trello-title-que-es-la-devscola',"https://api.trello.com/1/cards/DzDGRZwG?fields=all")
    this.xhrCallDescription('trello-description-que-es-la-devscola',"https://api.trello.com/1/cards/DzDGRZwG?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")

    //Que hacemos en la devscola
    this.xhrCallTitle('trello-title-que-hacemos',"https://api.trello.com/1/cards/fe3yBYxE?fields=all")

    //lista que hacemos en la devscola
    this.xhrCallAttachment('trello-attachment-katayunos',"https://api.trello.com/1/cards/XDf82tuh?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-katayunos',"https://api.trello.com/1/cards/XDf82tuh?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-katayunos',"https://api.trello.com/1/cards/XDf82tuh?fields=all")

    this.xhrCallAttachment('trello-attachment-datascience',"https://api.trello.com/1/cards/FmpEZHmm?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-datascience',"https://api.trello.com/1/cards/FmpEZHmm?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-datascience',"https://api.trello.com/1/cards/FmpEZHmm?fields=all")

    this.xhrCallAttachment('trello-attachment-devops',"https://api.trello.com/1/cards/H4hbOfsR?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-devops',"https://api.trello.com/1/cards/H4hbOfsR?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-devops',"https://api.trello.com/1/cards/H4hbOfsR?fields=all")

    this.xhrCallAttachment('trello-attachment-patrones',"https://api.trello.com/1/cards/zPTMDAAY?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-patrones',"https://api.trello.com/1/cards/zPTMDAAY?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-patrones',"https://api.trello.com/1/cards/zPTMDAAY?fields=all")

    //aprende a programar
    this.xhrCallTitle('trello-title-aprende-a-programar',"https://api.trello.com/1/cards/EHVElcVR?fields=all")
    this.xhrCallDescription('trello-description-aprende-a-programar',"https://api.trello.com/1/cards/EHVElcVR?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")

    //lista aprende a programar
    this.xhrCallAttachment('trello-attachment-run',"https://api.trello.com/1/cards/D32NEoDW?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-run',"https://api.trello.com/1/cards/D32NEoDW?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-run',"https://api.trello.com/1/cards/D32NEoDW?fields=all")

    this.xhrCallAttachment('trello-attachment-materiales',"https://api.trello.com/1/cards/DD8tAxPL?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-materiales',"https://api.trello.com/1/cards/DD8tAxPL?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-materiales',"https://api.trello.com/1/cards/DD8tAxPL?fields=all")

    this.xhrCallAttachment('trello-attachment-mentorias',"https://api.trello.com/1/cards/mFCba7qf?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-mentorias',"https://api.trello.com/1/cards/mFCba7qf?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-mentorias',"https://api.trello.com/1/cards/mFCba7qf?fields=all")

    this.xhrCallAttachment('trello-attachment-contenido-audiovisual',"https://api.trello.com/1/cards/tBkXwpRt?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallDescription('trello-description-contenido-audiovisual',"https://api.trello.com/1/cards/tBkXwpRt?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")
    this.xhrCallTitle('trello-title-contenido-audiovisual',"https://api.trello.com/1/cards/tBkXwpRt?fields=all")

    //participa
    this.xhrCallTitle('trello-title-participa',"https://api.trello.com/1/cards/yPv2wRBl?fields=all")
    this.xhrCallDescription('trello-description-participa',"https://api.trello.com/1/cards/yPv2wRBl?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")

    //donaciones
    this.xhrCallTitle('trello-title-donaciones',"https://api.trello.com/1/cards/Z4Z6Osa7?fields=all")
    this.xhrCallDescription('trello-description-donaciones',"https://api.trello.com/1/cards/Z4Z6Osa7?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")

    //footer

    //contacta
    this.xhrCallDescription('trello-description-contacta',"https://api.trello.com/1/cards/FLX8cIK4?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")

    //redes sociales
    this.xhrCallLinkWithIcon('trello-title-redes-sociales','https://api.trello.com/1/cards/btegl8k1?attachments=true&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false')

    //direccion
    this.xhrCallTitle('trello-title-direccion',"https://api.trello.com/1/cards/GzWzDwuS?attachments=false&attachment_fields=all&members=false&membersVoted=false&checkItemStates=false&checklists=none&checklist_fields=all&board=false&list=false&pluginData=false&stickers=false&sticker_fields=all&customFieldItems=false")


  }

  createTitle(elementId, response){
    let container = document.getElementById(elementId)
    let title = response.name
    console.log(response.name)
    container.append(title)
  }

  createDescription(elementId, response){
    const markdown = new Remarkable()
    const container = document.getElementById(elementId)
    const description = document.createElement('div')
    description.innerHTML = markdown.render(response.desc)

    container.append(description)
  }

  createAttachment(elementId, response){
    if (response.attachments == undefined){
      return
    }
    let attach = response.attachments[0].url
    let container = document.getElementById(elementId).innerHTML = `<img src='${attach}'/>`
  }

  xhrCallTitle(elementId, url){
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
      if (xhr.readyState === xhr.DONE) {
        let response = JSON.parse(xhr.responseText)

        this.createTitle(elementId, response)

      }
    }.bind(this))
    xhr.open('GET',url)
    xhr.send()
  }

  xhrCallDescription(elementId, url){
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
      if (xhr.readyState === xhr.DONE) {
        let response = JSON.parse(xhr.responseText)

        this.createDescription(elementId, response)

      }
    }.bind(this))
    xhr.open('GET',url)
    xhr.send()
  }

  xhrCallAttachment(elementId, url){
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
      if (xhr.readyState === xhr.DONE) {
        let response = JSON.parse(xhr.responseText)

        this.createAttachment(elementId, response)

      }
    }.bind(this))
    xhr.open('GET',url)
    xhr.send()
  }

  xhrCallLinkWithIcon(elementId, url){

    var xhr = new XMLHttpRequest();
    xhr.addEventListener("readystatechange", function () {
      if (xhr.readyState === xhr.DONE) {
        let response = JSON.parse(xhr.responseText)

        const href = response.desc
        const attachment = response.attachments[0]
        this.createLinkWithIcon(elementId, href, attachment.url)

      }
    }.bind(this))
    xhr.open('GET',url)
    xhr.send()

  }

  createLinkWithIcon(elementId, href, icon) {
    const container = document.querySelector('#' + elementId)
    const link = document.createElement('a')
    link.href = href
    const image = document.createElement('img')
    image.src = icon
    link.append(image)
    container.append(link)
  }
}

export default Page
