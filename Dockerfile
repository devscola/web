FROM node:12.3-alpine

WORKDIR /opt/app

RUN npm install http-server -g

COPY package.json package-lock.json /opt/app/

RUN npm install

COPY . /opt/app

RUN npm run build

CMD ["http-server", "/opt/app"]
