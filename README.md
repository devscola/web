# Web Devscola 2.0

## Desarrollo en local

### Requisitos

- nodejs
- npm

### Instalación (Mirar package.json sección scripts)

> $ npm install
> $ npm run build

En otra ventana:

> $ npm run build-watch


## Desarrollo con Docker

### Requisitos

- docker

### Instalación

> $ docker-compose up --build

Y en otra pestaña

> $ docker-compose exec web npm run build-watch

Ejecutar todos los pasos de nuevo cada vez que se cambie el código JavaScript.